from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL


# Create your models here.
class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateTimeField(auto_now=True)
    # TODO_check to see if you can add future date.
    owner = models.ForeignKey(USER_MODEL, on_delete=models.CASCADE)
    recipes = models.ManyToManyField(
        "recipes.Recipe", related_name="meal_plans", blank=True
    )

    def __str__(self):
        return f"{self.name} by {self.owner}"
